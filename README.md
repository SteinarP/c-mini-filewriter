Compile
See makefile at src, or:

    gcc ./src/writer.c -o ./out/writer
    gcc ./src/tester.c -o ./out/tester

First argument of writer will to where stdin is written (new or existing filepath). If none provided, defaults to *./out/output*.

Try running *./out/tester* to see if all 4 file copies are successful.

This version has been tested on Linux, not yet Windows.