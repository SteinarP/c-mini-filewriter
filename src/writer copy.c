/**
 * Author: Steinar Valheim Pedersen
 * Desc: A program writing standard input to a file provided by first argument.
 * Version: 1.2
 */


#ifdef _WIN32
# include <io.h>
# include <fcntl.h>
# define SET_BINARY_MODE(handle) setmode(handle, O_BINARY)
#else
# define SET_BINARY_MODE(handle) ((void)0)
#endif

// Includes
#include "writer.h"

// Defines
#define NRM "\x1B[0m"
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"

// Settings
int do_append = 0;
int file_already_existed = 1;
int total_writes = 0;

// Global required for access from signal handler.
// Cannot rely fully on returned values as we're expecting interruption.
FILE* output_file;
char* output_path;

int main(int argc, char** argv){ 
  // Parse argv
  if (argc > 1){
    if (argv[1][0]=='-'){
      printf("Cool\n");
    }
    output_path = argv[1];
  } else {
    output_path = DEFAULT_OUTPUT_PATH;
  }

  output_file = open_output_file(output_path);
  signal(SIGINT, INT_handler);

  // Write stdin to file

  write_to_output(output_file);
  safe_exit(EXIT_SUCCESS);
}

int file_exists(const char* path){
  return (access(path, F_OK) != -1);
}

FILE* open_output_file(char* filepath){
  file_already_existed = file_exists(filepath);
  //printf("Exists already: %d\n", file_already_existed);

  FILE *fp = fopen(filepath, "wb+");

  if (fp == NULL){
    if (errno == EACCES){
      fprintf(stderr, "Permission denied at filepath: '%s%s%s'\n", RED, filepath, NRM);
      exit(EXIT_FAILURE);
    } else {
      fprintf(stderr, "Error accessing filepath: '%s%s%s'\n (%d:%s)\n", RED, filepath, NRM, errno, strerror(errno));
      exit(EXIT_FAILURE);
    }
    return NULL;
  }
  // All well.
  if (file_already_existed){
    if (do_append){
      printf("Appending data to existing file \"%s%s%s\"",YEL, filepath, NRM);
    } else {
      printf("Overwriting existing file \"%s%s%s\"",YEL, filepath, NRM);
    }
  } else {
    printf("Created new file \"%s%s%s\". Now writing to it",YEL, filepath, NRM);
  }
  printf(" from standard input (stdin) of \"%s"__FILE__"%s\" until interrupted (CTRL+C).\n", YEL, NRM);
  return fp;
}

void write_to_output(FILE* output_file){
  size_t count;
  if (output_file == NULL){
    output_file = open_output_file(DEFAULT_OUTPUT_PATH);
  }
  // _setmode(_fileno(stdin), _O_BINARY);
  //void *input_buffer = malloc(BUF_LEN);

  //There's much redundant code here as I first tried to develop this
  //functionality on Windows, where EOF, filestreams, interruptions etc
  //seemed to work differently than on Unix.

  /*
  count = fread(input_buffer, 1, BUF_LEN, stdin);
      printf("Count: %d\n", count);
  while (count){
    printf("Total bytes written: %d\n", total_writes);
    fwrite(input_buffer, 1, count, fp);
    count = fread(input_buffer, 1, BUF_LEN, stdin);
    total_writes += count;
  }
  */


  /*
  while (1){
    int c = getc(stdin);
    if (c == EOF){
      printf("\nILLEGAL CHAR: ----> %c, %d, %02x\n", c, c, c);
      break;
    } else {
      total_writes++;
      fwrite(&c, 1, 1, fp);  
    }
  }
  */

  /*
    int c; 
    while (1){
      //putchar(c);
      //fprintf(fp, "%c", c);
      c = getchar();
      if (c == EOF){
        break;
      }
      //fputc(c, fp);
      fwrite(&c, 1, 1, output_file);
      //write(STDOUT_FILENO, &c, 1);
      total_writes++;
      //printf("Wrote %c to file, now %d\n", c, total_writes);
    }
  //printf("Finished writing at: c=%d, %c, %02x", c, c, c);
  */

  char buffer[BUF_LEN];
  //  _setmode(_fileno(stdout), _O_BINARY);
  // Forced one byte at a time. Otherwise keyboard input interrupt would
  // not submit to file. Seemed to work fine for windows though!
  // This is indeed not ideal for speed. TODO: Enable buffering.
  while ((count = fread(buffer, 1, 1/*sizeof(buffer)*/, stdin)) != 0){
      fwrite(buffer, 1, count, output_file);
      total_writes++;
  }

  // Done reading
  //free(input_buffer);

}

void safe_exit(int exit_code){
  fflush(stdin);
  if (output_file != NULL){
    fclose(output_file); 
  }

  if (total_writes > 0){
    printf("> Wrote %d times.\n", total_writes);
  } else {
    //Clean up any empty new file.
    if (!file_already_existed){
      printf("> Nothing written. Removing unused file.\n");
      remove(output_path); 
    }
    printf("> Exiting with no changes made.\n");
  }
  

  exit(exit_code);
}

void INT_handler(int sig){
  // Ignore interruption, terminating safely. 
  signal(sig, SIG_IGN); 
  safe_exit(EXIT_SUCCESS);
}