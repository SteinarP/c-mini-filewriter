// Includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

// Macro constants
#define BUF_LEN 64 * 1024 
#define DEFAULT_OUTPUT_PATH "./out/output"

// Function declarations
int file_exists(const char* path);
FILE* open_output_file(char* path);
void write_to_output();
void INT_handler(int);
void safe_exit();