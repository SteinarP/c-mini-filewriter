#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <errno.h>
#include <string.h>

#if defined(_WIN32) || defined(WIN32)
  #define PROG "writer.exe "
  #define ISWIN 1
#else 
  #define PROG "./out/writer "
  #define ISWIN 0
#endif

// Defines
#define NRM "\x1B[0m"
#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"

int send_to_writer();
int test_argv(int expected, const char* command_line, const char* input_after);
int assert_equal(int expected, int actual);
int equal_fcontent(const char* file1, const char* file2);
int test_copy(const char* new_file, const char* source);

int main(){
  int do_pipe_tests = 0;
  printf("Running tests...\n");

  if (ISWIN){
      test_argv(EXIT_SUCCESS, PROG, "Writing to default file path.");
      test_argv(EXIT_SUCCESS, PROG" a_new_file", "This is written at a new filepath!");
      test_argv(EXIT_FAILURE, PROG" .", "This should be impossible.");
      test_argv(EXIT_FAILURE, PROG" CON", "Windows disallows this filename");
      //Win: Try to pipe first file's content into second.
      
      // For some reason the "Get-Content works fine in Windows terminal stand but not when called from here."
      //test_argv(EXIT_SUCCESS, PROG" file_first", "This data can be copied at launch.");
      //test_argv(EXIT_SUCCESS, "Get-Content file_first | "PROG" file_second", "Written after launch");
  } else {
      //Unix: Try to pipe first file's content into second.
      //test_argv(EXIT_SUCCESS, PROG" file_first", "This data can be copied at launch.");
      //test_argv(EXIT_SUCCESS, PROG" file_second < cat file_first", "Written after launch");

      /*system("./out/writer FIRST_FILE");
      FILE *testable = popen("./out/writer FIRST_FILE", "wb");
      fprintf(testable, "%s\n", "Hello world!");
      fclose(testable);
      */

      //Copy small text file
      system("./out/writer ./out/hello_COPY < ./res/hello");
      int equality = equal_fcontent("./res/hello", "./out/hello_COPY");
      printf("hello_COPY and hello are equal?: %d\n", equality);

      //Copy big text file
      system("./out/writer ./out/corncob_COPY.txt < ./res/corncob_lowercase.txt");
      equality = equal_fcontent("./res/corncob_lowercase.txt", "./out/corncob_COPY.txt");
      printf("corncob_COPY.txt and corncob_lowercase.txt are equal?: %d\n", equality);

      //Copy big text file
      system("./out/writer ./out/bg_COPY.jpg < ./res/bg.jpg");
      equality = equal_fcontent("./res/bg.jpg", "./out/bg_COPY.jpg");
      printf("bg_COPY.jpg and bg.jpg are equal?: %d\n", equality);

      //Copy big text file
      system("./out/writer ./out/tester_COPY < ./out/tester");
      equality = equal_fcontent("./out/tester", "./out/tester_COPY");
      printf("./out/tester_COPY and ./out/tester are equal?: %d\n", equality);
      
      test_copy("abc", "def");
  }


  //printf("Files are equal: %d", equal_fcontent("out/writer.c", "writer-copy.c") );

  printf("\nAll tests passed!\n");
}

int test_copy(const char* new_file, const char* source){
  char *str;
  strcpy(str, "./out/writer ");
  //system("./out/writer ./out/tester_COPY < ./out/tester");
}

int basic_test(int expected_exit_code, const char* full_cmd){
  system(full_cmd);
}

int test_argv(int expected_exit_code, const char* command_line, const char* input_after){
  clock_t t = clock(); 
  
  FILE* testable = popen(command_line, "wb");
  fwrite(input_after, sizeof(char), strlen(input_after)+1, testable);
  //fprintf(testable, input_after); //%s?
  int actual_exit_code = pclose(testable);

  t = clock() - t; 
  double time_taken = t / (double)CLOCKS_PER_SEC;
  int ms = (int)(1000*time_taken);

  if (expected_exit_code == actual_exit_code){
    printf("%s[OK] Test passed: %d%s (%d ms)\n", GRN, actual_exit_code, NRM, ms);
    return 1;
  } else {
    printf("%s[!] Test FAILED! Expected '%d' but got '%d'%s %s (%d ms)\n", 
      RED, expected_exit_code, actual_exit_code, NRM, (char*)strerror(errno), ms);
    exit(EXIT_FAILURE);
  }
}

int equal_fcontent(const char* file1, const char* file2){
  FILE* fp1 = fopen(file1, "r");
  FILE* fp2 = fopen(file2, "r");
  if (fp1 == NULL){
    fprintf(stderr, "Couldn't open %s\n", file1);
    perror("equal_fcontent()");
    return 1;
  }
    if (fp2 == NULL){
    fprintf(stderr, "Couldn't open %s\n", file2);
    perror("equal_fcontent()");
    return 2;
  }
  if (fp1 == NULL || fp2 == NULL){
    perror("File not found.");
    return 0;
  }

  int byte1 = getc(fp1);
  int byte2 = getc(fp2);
  while ((byte1 != EOF) && (byte2 != EOF) && (byte1 == byte2)){
    byte1 = getc(fp1);
    byte2 = getc(fp2);
  }

  int equal = (byte1 == byte2);
  fclose(fp1);
  fclose(fp2);
  return equal;
}
