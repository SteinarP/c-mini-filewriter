#include <stdio.h>
//#include <io.h>
#include <fcntl.h>

int main()
{
    char buffer[1024];
    int count;

    FILE * fp = fopen("/out/output", "wb");
    if (fp == NULL){
        printf("fopen failed!\n");
    }

    //_setmode(_fileno(stdin), _O_BINARY);
    while ((count = fread(buffer, 1, sizeof(buffer), stdin)) != 0){
        fwrite(buffer, 1, count, fp);
        //printf("Wrote\n");
    }

    //printf("Done.\n");
    fclose(fp);
    return 0;
}